# README #

This repository defines the Logic layer between Phaos Games and their back-end data services. It is intended to be used to build Presentation layer components.

### How do I get set up? ###

You'll need to download the source code and compile it into a DLL. These interfaces can be used to create mock services that provide hardcoded results to the Presentation components so they can be easily tested.

See: http://docs.unity3d.com/Manual/UsingDLL.html

### Contribution guidelines ###

Contributions are welcomed. If it seems to make sense an API should be written differently, or additional functionality is needed. Feel free to create a fork and submit a suggested code update.