﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PhaosGames
{
    public interface IPlayerService
    {
        // <summary>
        // Used to retrieve the friends of a player. These friends may be from the game's 
        // personal database or a social network like Facebook or Google+.
        // </summary>
        IPlayer[] GetFriends(IPlayer player);
    }
}
