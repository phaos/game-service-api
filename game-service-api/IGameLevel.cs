﻿
namespace PhaosGames
{
    // <summary>Container for information related to game levels as defined by Phaos Games<summary>
    public interface IGameLevel
    {
        // <summary>The scene number as built in Unity. Used for sequencing the levels in linear progression.</summary>
        int GetSequence();
        // <summary>The name of the level, used to load levels via Unity API</summary>
        // <remarks>Analagous to Unity Scene name. For example, LevelName.unity</summary>
        string GetName();
    }
}
