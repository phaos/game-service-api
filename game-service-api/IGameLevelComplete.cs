﻿
namespace PhaosGames
{
    // <summary>Provides additional information about game level in context of it being completed by a player.</summary>
    // <remarks>The player this completed level object assumes is defined by the context it is created.</remarks>
    // <see cref="PhaosGames.IGameLevelService"/>
    public interface IGameLevelComplete : IGameLevel
    {
        // <summary>Used to determine a player's performance on a given level.</summary>
        IGameLevelScore GetScore();
    }


}
