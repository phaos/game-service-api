﻿using System;
using System.Collections.Generic;

namespace PhaosGames
{
    public interface IPlayer
    {
        // <summary>
        // Used to identify a player within a Phaos Game. This id is unique and synthetic.
        // </summary>
        // <returns>Player's unique identifier</returns>
        string GetId();
    }
}
