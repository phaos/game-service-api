﻿
namespace PhaosGames
{
    // <summary>Service responsible for business logic regarding the levels in a game.</summary>
    public interface IGameLevelService
    {
        // <summary>Used to disover available levels.</summary>
        // <remarks>Ruturned in the level order. Each index corresponds to the level number/sequence.</remarks>
        IGameLevel[] GetLevels();

        // <summary>Used for getting the levels a player has already completed successfully. This does not include levels that have been attempted but failed.</summary>
        // <remarks>Levels array is in the order they appear in the game.</remarks>
        IGameLevelComplete[] GetCompletedLevels(IPlayer player);

        // <summary>An interface to loading a game level. Expected to be called in favor of the Unity engine's load API to allow for custom analytics</summary>
        void LoadLevel(IGameLevel level);
    }
}
